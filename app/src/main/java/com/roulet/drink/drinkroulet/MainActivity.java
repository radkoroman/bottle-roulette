package com.roulet.drink.drinkroulet;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private ImageView bottleImg;
    private ImageView background;
    private ViewGroup transitionsContainer;
    private TextView message;
    private SensorManager msensorManager; //Менеджер сенсоров аппрата

    private float[] OrientationData;
    private float prevValue;
    private float prevTimestamp;
    private boolean firstTime = true;
    private float prevSpeed = 9999;
    private ObjectAnimator animator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bottleImg = findViewById(R.id.image_roulette);
        background = findViewById(R.id.background);
        transitionsContainer = findViewById(R.id.transition_container);
        message = findViewById(R.id.time_to_drink_msg);
        msensorManager = (SensorManager) getSystemService(this.SENSOR_SERVICE);
        OrientationData = new float[3];
    }

    @Override
    protected void onResume() {
        super.onResume();
        msensorManager.registerListener(this, msensorManager.getDefaultSensor(Sensor.TYPE_GAME_ROTATION_VECTOR), SensorManager.SENSOR_DELAY_UI);
    }

    public void actionRoulette(View view, boolean isRight) {
        hideSystemUI();
        int corner = 360 / 38;
        int randPosition = corner * new Random().nextInt(38); // random point
        int MIN = 5; // min rotation
        int MAX = 9; // max rotation
        long TIME_IN_WHEEL = 1000;  // time in one rotation
        int randRotation = MIN + new Random().nextInt(MAX - MIN); // random rotation
        int truePosition = randRotation * 360 + randPosition;
        long totalTime = TIME_IN_WHEEL * randRotation + (TIME_IN_WHEEL / 360) * randPosition;

        Log.d("ROULETTE_ACTION", "randPosition : " + randPosition
                + " randRotation : " + randRotation
                + " totalTime : " + totalTime
                + " truePosition : " + truePosition);

        animator = ObjectAnimator.ofFloat(view, "rotation", 0f, isRight ? (float) truePosition : -(float) truePosition);
        animator.setDuration(totalTime);
        animator.setInterpolator(new DecelerateInterpolator());
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                hideTimeToDrinkMsg();
                bottleImg.setEnabled(false);
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                showTimeToDrinkMsg();
                bottleImg.setEnabled(true);
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        animator.start();

    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        OrientationData = sensorEvent.values;
        float speed = 0;
        if (firstTime) {
            firstTime = false;
            prevTimestamp = sensorEvent.timestamp;
            prevValue = OrientationData[2] * 10;
        } else {
            speed = OrientationData[2] * 10 - prevValue;
        }
        if (prevSpeed != 9999 && Math.abs(prevSpeed - speed) > 0.5) {
            if (animator == null || !animator.isRunning()) {
                if (speed > 0) {
                    Log.d("Aksarrow", "slowering rotation left");
                    actionRoulette(bottleImg, true);
                } else {
                    actionRoulette(bottleImg, false);
                    Log.d("Aksarrow", "slowering rotation right");
                }
            }
        }
        prevSpeed = speed;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    private void showTimeToDrinkMsg() {
        TransitionManager.beginDelayedTransition(transitionsContainer);
        message.setVisibility(View.VISIBLE);
        background.setVisibility(View.VISIBLE);
    }

    private void hideTimeToDrinkMsg() {
        TransitionManager.beginDelayedTransition(transitionsContainer);
        message.setVisibility(View.GONE);
        background.setVisibility(View.GONE);
    }

    // This snippet hides the system bars.
    private void hideSystemUI() {
        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
    }
}
